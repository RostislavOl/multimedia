import cv2


image = cv2.imread("pic.jpg")
cv2.imshow("original", image)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray = cv2.GaussianBlur(gray, (3, 3), 0)
cv2.imwrite("graypic.jpg", gray)
cv2.imshow("graypic", gray)
ret, edged = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY)
cv2.imwrite("edgedpic.jpg", edged)
cv2.imshow("edgedpic", edged)
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
cv2.imwrite("closepic.jpg", closed)
cv2.imshow("closepic", closed)
#cnts = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[1]
#cnts = cv2.findContours(closed.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[1]
cnts = cv2.findContours(closed.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[1]

for c in cnts:
    # аппроксимируем (сглаживаем) контур
    peri = cv2.arcLength(c, True)
    approx = cv2.approxPolyDP(c, 0.02 * peri, True)
    cv2.drawContours(image, [approx], -1, (0, 255, 0), 4)

cv2.imwrite("newpic.jpg", image)
cv2.imshow("newpic", image)

cv2.waitKey(0)