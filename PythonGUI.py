from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import numpy as np
import cv2


def edit_click():
    messagebox.showinfo("GUI Python", "Нажата опция Edit")

root = Tk()
root.title("GUI на Python")
root.geometry("300x250")

def initial_image():
     global imagine
     img = filedialog.askopenfilename(title="choose your file",
                                                filetypes=(("jpeg files", "*.jpg"), ("all files", "*.*")))
     img = img.replace(img[2], '\\')
     imagine = cv2.imread(img)
     cv2.imshow("Initial Image", imagine)
     global gray
     gray = cv2.cvtColor(imagine, cv2.COLOR_BGR2GRAY)

def Filter2d():
    kernel = np.ones((16, 16), np.float32) / 256
    global dst
    dst = cv2.filter2D(gray, -1, kernel)
    cv2.imwrite("dstpic.jpg", dst)
    cv2.imshow("2D Image", dst)

def blur():
    global blur
    blur = cv2.blur(gray, (16, 16))
    cv2.imwrite("blur.jpg", blur)
    cv2.imshow("Blur image", blur)

def MedianFilter():
    global median
    median = cv2.medianBlur(gray, 9)
    cv2.imwrite("medianpic.jpg", median)
    cv2.imshow("Median Blure", median)

def GaussinBlur():
    global blurg
    blurg = cv2.GaussianBlur(gray, (5, 5), 0)
    cv2.imwrite("blurgpic.jpg", blurg)
    cv2.imshow("Gaussian Blure", blurg)

def erode():
    global erosion
    kernel = np.ones((5, 5), np.uint8)
    erosion = cv2.erode(gray, kernel, iterations=1)
    cv2.imwrite("ergpic.jpg", erosion)
    cv2.imshow("Erode", erosion)

def dilate():
    global dilation
    kernel = np.ones((5, 5), np.uint8)
    dilation = cv2.dilate(gray, kernel, iterations=1)
    cv2.imwrite("dilpic.jpg", dilation)
    cv2.imshow("Dilatate", dilation)

def sobel():
    global sobelx
    global sobelxy
    global sobely
    sobelx = cv2.Sobel(gray,cv2.CV_64F, 1 ,0, ksize=15)
    sobely = cv2.Sobel(gray,cv2.CV_64F, 0, 1, ksize=15)
    sobelxy = cv2.Sobel(gray,cv2.CV_64F, 1, 1, ksize=5)
    cv2.imwrite("sobelpic.jpg", sobelxy)
    cv2.imshow("Sobel", sobelxy)

def Laplacian():
    global laplacian
    laplacian = cv2.Laplacian(gray, cv2.CV_64F)
    cv2.imwrite("laplacianpic.jpg", laplacian)
    cv2.imshow("Laplacian", laplacian)

def Canny():
    global edges
    edges = cv2.Canny(gray, 100, 200)
    cv2.imwrite("edgepic.jpg", edges)
    cv2.imshow("Canny", edges)

def calcHist():

    global his

    his = np.zeros((600, 656, 3))

    bins = np.arange(256).reshape(256, 1)
    color = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]

    for ch, col in enumerate(color):
        hist_item = cv2.calcHist([imagine], [ch], None, [256], [0, 255])
        cv2.normalize(hist_item, hist_item, 0, 255, cv2.NORM_MINMAX)
        hist = np.int32(np.around(hist_item))
        pts = np.column_stack((bins, hist))
        cv2.polylines(his, [pts], False, col)
    his = np.flipud(his)
    cv2.imwrite("hispic.jpg", his)
    cv2.imshow("calcHist", his)

def equalizeHist():
    global res
    equ = cv2.equalizeHist(gray)
    res = np.hstack((gray, equ))
    cv2.imwrite("respic.jpg", res)
    cv2.imshow("Equalize Hist", res)

main_menu = Menu()

file_menu = Menu()

file_menu.add_command(label="Initial image", command=initial_image)
file_menu.add_command(label="Filter2d", command=Filter2d)
file_menu.add_command(label="Blur", command=blur)
file_menu.add_command(label="Median Blur", command=MedianFilter)
file_menu.add_command(label="Gaussian Blur", command=GaussinBlur)
file_menu.add_command(label="Erode", command=erode)
file_menu.add_command(label="Dilate", command=dilate)
file_menu.add_command(label="Sobel", command=sobel)
file_menu.add_command(label="Laplacian", command=Laplacian)
file_menu.add_command(label="Canny", command=Canny)
file_menu.add_command(label="Calc Hist", command=calcHist)
file_menu.add_command(label="Equalize Hist", command=equalizeHist)

main_menu.add_cascade(label="File", menu=file_menu)

def new_window(event):
    initial_image()
    window = Toplevel(root)

    window.config(menu=main_menu)

but = Button(root, text='picture')
but.bind('<Button->', new_window)

but.pack()
root.mainloop()