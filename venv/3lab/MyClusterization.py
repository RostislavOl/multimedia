from tkinter import *
from tkinter import filedialog
import cv2
import numpy as np
import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plot

root = Tk()
root.title("GUI на Python")
root.geometry("300x450")

def start_clusterizations():
    global file
    global clusterCount
    print(clusterCount)
    criterions = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    file = filedialog.askopenfilename(title="choose your file",
                                      filetypes=(("yml files", "*.yml"), ("all files", "*.*")))
    global storage
    storage = cv2.FileStorage(file, cv2.FILE_STORAGE_READ)
    #global dataset
    dataset = storage.getNode('points').mat()
    print(dataset)
    storage.release()
    ret, label, center = cv2.kmeans(dataset, clusterCount, None, criterions, 10, cv2.KMEANS_RANDOM_CENTERS)
    colors = ["r", "g", "b", "y", "m", "c"]
    for i in range(clusterCount):
        cluster = dataset[label.ravel() == i]
        plot.scatter(cluster[:, 0], cluster[:, 1], c=colors[i])
        plot.scatter(center[:, 0], center[:, 1], s=80, c='y', marker='s')

    plot.xlabel('Height'), plot.ylabel('Weight')
    plot.show()

def add():
    global clusterCount
    clusterCount = clusters.get()
    print(clusterCount)



label1 = Label(root, text="Введите количество кластеров").grid(row=0, sticky=W)
clusters = IntVar()
global clusterCount
clusters_entry = Entry(textvariable=clusters).grid(row=1, sticky=W)
but = Button(text='Внести', command=add).grid(row=2, sticky=W)
but = Button(text='Построить', command=start_clusterizations).grid(row=3, sticky=W)



root.mainloop()