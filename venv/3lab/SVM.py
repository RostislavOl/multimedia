import cv2
import ClassificationsOperations as Base

class SVM(Base.ClassificationsOperations):
    def __init__(self, svm_type, kernel_type, degree, gamma, c, nu):
        model = cv2.ml.SVM_create()
        super().__init__(model)
        self.model.setType(svm_type)
        self.model.setKernel(kernel_type)
        self.model.setDegree(degree)
        self.model.setGamma(gamma)
        self.model.setC(c)
        self.model.setNu(nu)

    def train(self, samples, responses):
        return self.model.train(samples, cv2.ml.ROW_SAMPLE, responses)
