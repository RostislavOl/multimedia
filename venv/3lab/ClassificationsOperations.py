import numpy
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt

class ClassificationsOperations:
    def __init__(self, model):
        self.model = model

    def predict(self, values):
        result = self.model.predict(values)
        return numpy.ravel(result[1])

    def error(self, features, responses):
        predictions = numpy.array(numpy.ravel(self.predict(features)), dtype=numpy.int)
        responses = numpy.ravel(responses)
        return 1 - numpy.sum(responses == predictions) / len(responses)

    def get_range(self, points):
        max = numpy.amax(points, axis=0)
        min = numpy.amin(points, axis=0)
        return min, max

    def drawPoints(self, points, responses):
        min, max = self.get_range(points)
        h = 0.02
        xx, yy = numpy.meshgrid(numpy.arange(min[0], max[0], h), numpy.arange(min[1], max[1], h))
        X_hypo = numpy.c_[xx.ravel().astype(numpy.float32), yy.ravel().astype(numpy.float32)]
        zz = self.predict(X_hypo)
        zz = zz.reshape(xx.shape)
        plt.contourf(xx, yy, zz, cmap=plt.cm.coolwarm, alpha=0.8)
        plt.scatter(points[:, 0], points[:, 1], c=numpy.ravel(responses), s=30)
        plt.show()
