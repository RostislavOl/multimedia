import cv2
import ClassificationsOperations as Base


class RTrees(Base.ClassificationsOperations):
    def __init__(self, max_depth, min_sample_count, max_iter):
        model = cv2.ml.RTrees_create()
        super().__init__(model)
        self.model.setMaxDepth(max_depth)
        self.model.setMinSampleCount(min_sample_count)
        term_criteria = (cv2.TERM_CRITERIA_MAX_ITER, max_iter, 1)
        self.model.setTermCriteria(term_criteria)

    def train(self, samples, responses):
        return self.model.train(samples, cv2.ml.ROW_SAMPLE, responses)
