from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import numpy as np
import cv2
import SVM
import DTree
import RTree

root = Tk()
root.title("GUI на Python")
root.geometry("600x650")


def initial_image():
    global file
    file = filedialog.askopenfilename(title="choose your file",
                                      filetypes=(("yml files", "*.yml"), ("all files", "*.*")))
    global file_storage
    global features_train_data
    global response_train_data
    global features_test_data
    global response_test_data
    file_storage = cv2.FileStorage(file, cv2.FILE_STORAGE_READ)
    features_train_data = file_storage.getNode('features_train').mat()
    response_train_data = file_storage.getNode('response_train').mat()
    features_test_data = file_storage.getNode('features_test').mat()
    response_test_data = file_storage.getNode('response_test').mat()
    file_storage.release()


def svm_window():
    global file_storage
    initial_image()
    global target_kernel_type
    global svm_type
    svm_type = cv2.ml.SVM_NU_SVC
    global degree
    degree = 1
    global gamma
    gamma = 2
    global c
    c = 1
    global nu
    nu = 0.5
    label1 = Label(root, text="Выбор типа машины опрных векторов").grid(row=0, sticky=W)
    kernel_list = [("SVM_C_SVC", 1), ("SVM_NU_SVC", 2), ("SVM_ONE_CLASS", 3), ("SVM_EPS_SVR", 4), ("SVM_NU_SVR", 5)]
    global var
    var = IntVar()

    row = 1
    for txt, val in kernel_list:
        Radiobutton(text=txt, value=val, variable=var, padx=15, pady=10, command=select_svm) \
            .grid(row=row, sticky=W)
        row += 1

    label2 = Label(text="Выбор типа ядра").grid(row=6, sticky=W)
    kernel_type_list = [("SVM_LINEAR", 1), ("SVM_POLY", 2), ("SVM_RBF", 3), ("SVM_SIGMOID", 4)]

    row = 7
    global vibor
    vibor = IntVar()
    for txt, val in kernel_type_list:
        Radiobutton(text=txt, value=val, variable=vibor, padx=15, pady=10, command=select_kernel) \
            .grid(row=row, sticky=W)
        row += 1
    but = Button(text='Go!', command=learnSVMtrigger).grid(row=20, sticky=E)


def select_kernel():
    x = vibor.get()
    print(x)
    global target_kernel_type
    global degree
    if vibor.get() == 1:
        target_kernel_type = cv2.ml.SVM_LINEAR
        degree = 1
    if vibor.get() == 2:
        target_kernel_type = cv2.ml.SVM_POLY
        showdegree()
        change_gamma()
    if vibor.get() == 3:
        target_kernel_type = cv2.ml.SVM_RBF
        degree = 1
        change_gamma()
    if vibor.get() == 4:
        target_kernel_type = cv2.ml.SVM_SIGMOID
        degree = 1
        change_gamma()

def showdegree():
    label2 = Label(text="Degree").grid(row=11, sticky=W)
    global message
    message = IntVar()
    degree_entry = Entry(textvariable=message).grid(row=12, sticky=W)
    but = Button(text='Внести', command=get_message).grid(row=13, sticky=W)

def get_message():
    global degree
    degree = message.get()
    print(degree)

def change_gamma():
    label5 = Label(text="Gamma").grid(row=21, sticky=W)
    global gammamessage
    gammamessage = IntVar()
    degree_entry = Entry(textvariable=gammamessage).grid(row=22, sticky=W)
    but = Button(text='Внести', command=get_gamma).grid(row=23, sticky=W)

def get_gamma():
    global gamma
    gamma = gammamessage.get()
    print(gamma)

def select_svm():
    l = var.get()
    global svm_type
    if l == 1:
        svm_type = cv2.ml.SVM_C_SVC
        change_c()
    if l == 2:
        svm_type = cv2.ml.SVM_NU_SVC
    if l == 3:
        svm_type = cv2.ml.SVM_ONE_CLASS
    if l == 4:
        svm_type = cv2.ml.SVM_EPS_SVR
        change_c()
    if l == 5:
        svm_type = cv2.ml.SVM_NU_SVR
        change_c()

def change_c():
    label3 = Label(text="C").grid(row=14, sticky=W)
    global ctext
    ctext = IntVar()
    degree_entry = Entry(textvariable=ctext).grid(row=15, sticky=W)
    but = Button(text='Внести', command=get_c).grid(row=16, sticky=W)

def get_c():
    global c
    c = ctext.get()
    print(c)

def change_nu():
    label4 = Label(text="C").grid(row=17, sticky=W)
    global nutext
    nutext = DoubleVar()
    degree_entry = Entry(textvariable=nutext).grid(row=18, sticky=W)
    but = Button(text='Внести', command=get_nu).grid(row=19, sticky=W)

def get_nu():
    global nu
    nu = nutext.get()

def learnSVMtrigger():
    global file_storage
    global features_train_data
    global response_train_data
    global features_test_data
    global response_test_data
    print(target_kernel_type)
    print("C =" + str(c))
    print("Nu = " + str(nu))
    print("degree = " + str(degree))
    print("gamma = " + str(gamma))
    svm = SVM.SVM(svm_type, target_kernel_type, degree, gamma, c, nu)
    svm.train(features_train_data, response_train_data)
    print("Ошибка на обучающей выборке: %f" % svm.error(features_train_data, response_train_data))
    print("Ошибка на тестовой выборке: %f" % svm.error(features_test_data, response_test_data))
    svm.drawPoints(features_train_data, response_train_data)

def dtree_window():
    initial_image()
    label1 = Label(text="Высота дерева").grid(row=0, sticky=W)
    global md
    global msc
    msc = 0
    md = 0
    global max_depth
    global min_sample_count
    max_depth = IntVar()
    min_sample_count = IntVar()
    max_depth_entry = Entry(textvariable=max_depth).grid(row=1, sticky=W)
    label2 = Label(text="Минимальное возможное количество элементов выборки").grid(row=2, sticky=W)
    min_sample_count_entry = Entry(textvariable=min_sample_count).grid(row=3, sticky=W)

    but = Button(text='Внести', command=add_depth).grid(row=4, sticky=W)

def add_depth():
    global file_storage
    global features_train_data
    global response_train_data
    global features_test_data
    global response_test_data
    md = max_depth.get()
    msc = min_sample_count.get()
    print(md)
    print(msc)
    d_tree = DTree.DTree(md, msc)
    d_tree.train(features_train_data, response_train_data)
    print("Ошибка на обучающей выборке: %f" % d_tree.error(features_train_data, response_train_data))
    print("Ошибка на тестовой выборке: %f" % d_tree.error(features_test_data, response_test_data))
    d_tree.drawPoints(features_train_data, response_train_data)
    #train
    #drawPoint

def rtree_window():
    initial_image()
    label1 = Label(text="Высота деревьев").grid(row=0, sticky=W)
    global md
    global msc
    global mi
    global max_depth
    global min_sample_count
    global max_iterations
    max_depth = IntVar()
    min_sample_count = IntVar()
    max_iterations = IntVar()
    max_depth_entry = Entry(textvariable=max_depth).grid(row=1, sticky=W)
    label4 = Label(text="Минимальное возможное количество элементов").grid(row=2, sticky=W)
    min_sample_count_entry = Entry(textvariable=min_sample_count).grid(row=3, sticky=W)
    label3 = Label(text="Кол-во деревьев").grid(row=4, sticky=W)
    max_iterations_entry = Entry(textvariable=max_iterations).grid(row=5, sticky=W)

    but = Button(text='Внести', command=add_depth_trees).grid(row=6, sticky=W)

def add_depth_trees():
    global file_storage
    global features_train_data
    global response_train_data
    global features_test_data
    global response_test_data
    md = max_depth.get()
    msc = min_sample_count.get()
    mi = max_iterations.get()
    print(md)
    print(msc)
    r_tree = RTree.RTrees(md, msc, mi)
    r_tree.train(features_train_data, response_train_data)
    print("Ошибка на обучающей выборке: %f" % r_tree.error(features_train_data, response_train_data))
    print("Ошибка на тестовой выборке: %f" % r_tree.error(features_test_data, response_test_data))
    r_tree.drawPoints(features_train_data, response_train_data)

def startClusterization():
    global file
    initial_image()
    clusterization_parser = ArgsParser
    clusterization_parser.get_training_data_file_path(file)
    clusterz = IntVar()
    label4 = Label(text="Количество кластеров").grid(row=0, sticky=W)
    clusterz_entry = Entry(textvariable=clusterz).grid(row=1, sticky=W)



main_menu = Menu()
file_menu = Menu()

file_menu.add_command(label="Add Dataset", command=initial_image)
file_menu.add_command(label="Learn SVM", command=svm_window)
file_menu.add_command(label="Learn RTree", command=rtree_window)
file_menu.add_command(label="Learn DTree", command=dtree_window)
file_menu.add_command(label="Clusterization", command=startClusterization)

main_menu.add_cascade(label="File", menu=file_menu)

root.config(menu=main_menu)
root.mainloop()
