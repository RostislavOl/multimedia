import cv2
import ClassificationsOperations as Base


class DTree(Base.ClassificationsOperations):
    def __init__(self, max_depth, min_sample_count):
        model = cv2.ml.DTrees_create()
        super().__init__(model)
        self.model.setMaxDepth(max_depth)
        self.model.setMinSampleCount(min_sample_count)
        self.model.setCVFolds(1)

    def train(self, samples, responses):
        return self.model.train(samples, cv2.ml.ROW_SAMPLE, responses)
