import argparse


class ArgsParser:
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("C://Datasets//airplanes", metavar="FIRST_CLASS_IMAGES_PATH", type=str, help="Full path to first class images")
        parser.add_argument("C://Datasets//brontosaurus", metavar="SECOND_CLASS_IMAGES_PATH", type=str, help="Full path to second class images")
        parser.add_argument("detector_type", metavar="DETECTOR_TYPE", type=str, help="Image key points detector")
        parser.add_argument("descriptor_type", metavar="DESCRIPTOR_TYPE", type=str, help="Image key points descriptors")
        parser.add_argument("voc_size", metavar="VOC_SIZE", type=int, help="Descriptors vocabulary size")
        parser.add_argument("train_proportion", metavar="TRAIN_PROPORTION", type=float, help="Train proportion of images")
        args = parser.parse_args()
        self.first_class_images_path = args.first_class_images_path
        self.second_class_images_path = args.second_class_images_path
        self.detector_type = args.detector_type
        self.descriptor_type = args.descriptor_type
        self.voc_size = args.voc_size
        self.train_proportion = args.train_proportion

    def get_first_class_images_path(self):
        return self.first_class_images_path

    def get_second_class_images_path(self):
        return self.second_class_images_path

    def get_detector_type(self):
        return self.detector_type

    def get_descriptor_type(self):
        return self.descriptor_type

    def get_voc_size(self):
        return self.voc_size

    def get_train_proportion(self):
        return self.train_proportion
